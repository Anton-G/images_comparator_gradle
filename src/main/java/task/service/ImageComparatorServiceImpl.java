package task.service;


import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import task.model.ChangeScope;
import task.model.Pixel;
import task.model.TestedImage;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ImageComparatorServiceImpl implements ImageComparatorService {

    private final static Logger LOGGER = Logger.getLogger(ImageComparatorServiceImpl.class.getName());

    private final static double BORDER = 0.1;

    @Override
    public List<Pixel> compareImages(TestedImage image1, TestedImage image2) {
        List<Pixel> result = new ArrayList<>();

        //construct pixel readers start
        PixelReader pReader1 = image1.getImage().getPixelReader();
        PixelReader pReader2 = image2.getImage().getPixelReader();
        //construct pixel readers finish

        //compare pixels between images start
        for (int y = 0; y < image1.getImage().getHeight(); y++) {
            for (int x = 0; x < image1.getImage().getWidth(); x++) {
                if (isPixelsDifferent(pReader1.getColor(x, y), pReader2.getColor(x, y))) {
                    result.add(new Pixel(x, y));
                }
            }
        }
        //compare pixels between images finish
        return result;
    }

    @Override
    public List<List<Pixel>> rearangeDifferentPixelsToRegions(List<Pixel> pixels) {
        List<List<Pixel>> result = new ArrayList<>();

        for (Pixel p : pixels) {
            if (result.isEmpty()) {
                List<Pixel> newCol = new ArrayList<>();
                newCol.add(p);
                result.add(newCol);
            } else {
                operatePixel(p, result);
            }
        }
        return result;
    }

    @Override
    public WritableImage markChangedZones(Image image, List<ChangeScope> scopes) {
        WritableImage wImage = new WritableImage((int) image.getWidth(), (int) image.getHeight());
        PixelWriter pixelWriter = wImage.getPixelWriter();

        recreateImage(image, pixelWriter);

        for (ChangeScope scope : scopes) {
            paintPixels(pixelWriter, scope);
        }
        return wImage;
    }

    @Override
    public ChangeScope makeScope(List<Pixel> pixels) {
        int minX = pixels.get(0).getX();
        int maxX = pixels.get(0).getX();
        int minY = pixels.get(0).getY();
        int maxY = pixels.get(0).getY();

        for (Pixel pixel : pixels) {

            if (pixel.getX() < minX) {
                minX = pixel.getX();
            }
            if (pixel.getX() > maxX) {
                maxX = pixel.getX();
            }

            if (pixel.getY() < minY) {
                minY = pixel.getY();
            }
            if (pixel.getY() > maxY) {
                maxY = pixel.getY();
            }
        }
        return new ChangeScope(maxX, minX, maxY, minY);
    }

    @Override
    public boolean isImagesHaveTheSameSize(Image i1, Image i2) {
        return i1.getHeight() == i2.getHeight() && i1.getWidth() == i2.getWidth();
    }

    @Override
    public Image uploadImage(String pathToImage) {
        if (!pathToImage.contains("http")) {
            pathToImage = "file:" + pathToImage;
        }
        return new Image(pathToImage, true);
    }

    @Override
    public void displayImage(Pane pane, Image image) {
        ImageView imageView = new ImageView();
        imageView.setImage(image);
        imageView.setFitWidth(600);
        imageView.setFitHeight(600);
        imageView.setPreserveRatio(true);
        pane.setBackground(null);
        pane.getChildren().clear();
        pane.getChildren().add(imageView);
    }

    @Override
    public void displayChangedImage(Pane pane, WritableImage image) {
        ImageView imageView = new ImageView();
        imageView.setImage(image);
//        imageView.setFitWidth(600);
//        imageView.setFitHeight(600);
        imageView.setPreserveRatio(true);
        pane.setBackground(null);
        pane.getChildren().clear();

        ScrollPane scrollPane = new ScrollPane(imageView);
        scrollPane.setPannable(true);
        scrollPane.prefWidthProperty().bind(image.widthProperty());
        scrollPane.prefHeightProperty().bind(image.heightProperty());

        pane.getChildren().add(scrollPane);
    }

    private void recreateImage(Image image, PixelWriter pixelWriter) {

        PixelReader pReader = image.getPixelReader();

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                pixelWriter.setColor(x, y, pReader.getColor(x, y));
            }
        }
    }

    private void paintPixels(PixelWriter pw, ChangeScope scope) {
        writeHLineWithPixels(pw, scope.getMinX(), scope.getMaxX(), scope.getMinY());
        writeHLineWithPixels(pw, scope.getMinX(), scope.getMaxX(), scope.getMaxY());

        writeVLineWithPixels(pw, scope.getMinY(), scope.getMaxY(), scope.getMinX());
        writeVLineWithPixels(pw, scope.getMinY(), scope.getMaxY(), scope.getMaxX());
    }

    private void writeHLineWithPixels(PixelWriter pw, int start, int end, int boundary) {
        for (int i = start; i <= end; i++) {
            pw.setColor(i, boundary, Color.RED);
        }
    }

    private void writeVLineWithPixels(PixelWriter pw, int start, int end, int boundary) {
        for (int i = start; i <= end; i++) {
            pw.setColor(boundary, i, Color.RED);
        }
    }

    private void operatePixel(Pixel p, List<List<Pixel>> result) {
        boolean isNeighborWasFound = false;
        for (List<Pixel> pixels : result) {
            for (Pixel pixel : pixels) {
                if (arePixelsNeighbors(pixel, p)) {
                    pixels.add(p);
                    isNeighborWasFound = true;
                    break;
                }
            }
        }

        if (!isNeighborWasFound) {
            List<Pixel> newCol = new ArrayList<>();
            newCol.add(p);
            result.add(newCol);
        }
    }

    private boolean arePixelsNeighbors(Pixel p1, Pixel p2) {
        if (p1.getX() == p2.getX() - 1 && p1.getY() == p2.getY()) {
            return true;
        } else if (p1.getX() == p2.getX() && p1.getY() == p2.getY() - 1) {
            return true;
        } else if (p1.getX() == p2.getX() - 1 && p1.getY() == p2.getY() - 1) {
            return true;
        } else if (p1.getX() == p2.getX() + 1 && p1.getY() == p2.getY() - 1) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isPixelsDifferent(Color c1, Color c2) {
        double redIndex = c1.getRed() / c2.getRed();
        double blueIndex = c1.getBlue() / c2.getBlue();
        double greenIndex = c1.getGreen() / c2.getGreen();

        if (redIndex != 1 && redIndex > BORDER) {
            return true;
        } else if (blueIndex != 1 && blueIndex > BORDER) {
            return true;
        } else if (greenIndex != 1 && greenIndex > BORDER) {
            return true;
        }
        return false;
    }
}
