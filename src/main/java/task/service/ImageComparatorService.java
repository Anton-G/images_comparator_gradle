package task.service;


import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import task.model.ChangeScope;
import task.model.Pixel;
import task.model.TestedImage;

import java.util.List;

public interface ImageComparatorService {

    /**
     * This method responsible for finding all different pixels between two images
     *
     * @param image1
     * @param image2
     * @return collection of pixels that are different
     */
    List<Pixel> compareImages(TestedImage image1, TestedImage image2);

    /**
     * This method responsible for find different pixels and if they are neighborhood
     * to make collection with that pixels.
     *
     * @param pixels
     * @return
     */
    List<List<Pixel>> rearangeDifferentPixelsToRegions(List<Pixel> pixels);

    /**
     * This method responsible for prepare image to show
     *
     * @param image
     * @param scopes
     * @return
     */
    WritableImage markChangedZones(Image image, List<ChangeScope> scopes);

    ChangeScope makeScope(List<Pixel> pixels);

    boolean isImagesHaveTheSameSize(Image i1, Image i2);

    Image uploadImage(String pathToImage);

    void displayImage(Pane pane, Image image);

    void displayChangedImage(Pane pane, WritableImage image);
}
