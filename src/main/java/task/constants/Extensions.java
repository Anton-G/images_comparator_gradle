package task.constants;


public enum Extensions {
    JPG("jpg"), JPEG("jpeg"), PNG("png"), BMP("bmp");

    private String value;

    Extensions(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
