package task.constants;


public enum MessagesComponents {

    WRONG_EXTENSION("Wrong file extension"),
    COMPARISON_RESULT("Comparison result"),
    MSG_UPLOAD_IMAGES("Please upload images before compare them"),
    MSG_CANNOT_COMPARE_DIF_SIZES("Images cannot be compared - they have different sizes"),
    MSG_IDENTICAL_IMAGES("The pictures are identical."),
    MSG_ALLOWED_EXTENSIONS_PART("File should have one of these extensions: "),
    COMMA(","),
    DOT(".");

    private String value;

    MessagesComponents(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
