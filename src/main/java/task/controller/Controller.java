package task.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import task.constants.Extensions;
import task.exception.ImagesComparatorException;
import task.model.ChangeScope;
import task.model.Pixel;
import task.model.TestedImage;
import task.service.ImageComparatorService;
import task.service.ImageComparatorServiceImpl;
import task.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static task.constants.MessagesComponents.COMMA;
import static task.constants.MessagesComponents.COMPARISON_RESULT;
import static task.constants.MessagesComponents.DOT;
import static task.constants.MessagesComponents.MSG_ALLOWED_EXTENSIONS_PART;
import static task.constants.MessagesComponents.MSG_CANNOT_COMPARE_DIF_SIZES;
import static task.constants.MessagesComponents.MSG_IDENTICAL_IMAGES;
import static task.constants.MessagesComponents.MSG_UPLOAD_IMAGES;
import static task.constants.MessagesComponents.WRONG_EXTENSION;


public class Controller {

    private final static Logger LOGGER = Logger.getLogger(Controller.class.getName());
    private static String allowedFileExtensionsMessage;
    private final int BOUNDARY = 200;
    @FXML
    private TextField firstImageLocation;
    @FXML
    private TextField secondImageLocation;
    @FXML
    private StackPane firstImagePane;
    @FXML
    private StackPane secondImagePane;

    private TestedImage firstImage;

    private TestedImage secondImage;

    private ImageComparatorService imageService = new ImageComparatorServiceImpl();

    @FXML
    protected void handleCaptureFirstImageLocation(ActionEvent event) {
        if (isPathShouldBeHandled(firstImageLocation)) {
            updatePathForWindows(firstImageLocation);
            firstImage = new TestedImage(firstImageLocation.getText());
            firstImage.setImage(imageService.uploadImage(firstImage.getImagePath()));
            firstImage.setFileExtension(Utils.getFileExtension(firstImageLocation.getText()));
            imageService.displayImage(firstImagePane, firstImage.getImage());
        } else {
            LOGGER.warning("File with wrong extension was passed");
            createPopUp(WRONG_EXTENSION.getValue(), this.getAllowedFileExtensionsMessage());
        }
    }

    private void updatePathForWindows(TextField path) {
        if (path.getText().contains("\\")) {
            path.getText().replace("\\", "\\\\");
        }
    }

    @FXML
    protected void handleCaptureSecondImageLocation(ActionEvent event) {
        if (isPathShouldBeHandled(secondImageLocation)) {
            updatePathForWindows(secondImageLocation);
            secondImage = new TestedImage(secondImageLocation.getText());
            secondImage.setImage(imageService.uploadImage(secondImageLocation.getText()));
            secondImage.setFileExtension(Utils.getFileExtension(secondImageLocation.getText()));
            imageService.displayImage(secondImagePane, secondImage.getImage());
        } else {
            createPopUp(WRONG_EXTENSION.getValue(), this.getAllowedFileExtensionsMessage());
        }
    }

    @FXML
    protected void handleCompareImages(ActionEvent event) {
        try {
            if (firstImage == null || secondImage == null || firstImage.getImage() == null || secondImage.getImage() == null) {
                throw new ImagesComparatorException(COMPARISON_RESULT.getValue(), MSG_UPLOAD_IMAGES.getValue());
            } else if (!imageService.isImagesHaveTheSameSize(firstImage.getImage(), secondImage.getImage())) {
                throw new ImagesComparatorException(COMPARISON_RESULT.getValue(), MSG_CANNOT_COMPARE_DIF_SIZES.getValue());
            }

            List<Pixel> pixels = imageService.compareImages(firstImage, secondImage);

            //todo exclude pixels that shouldn`t be compared start
            //code goes here
            //todo exclude pixels that shouldn`t be compared finish

            if (pixels.isEmpty()) {
                throw new ImagesComparatorException(COMPARISON_RESULT.getValue(), MSG_IDENTICAL_IMAGES.getValue());
            }

            List<List<Pixel>> pixelsRegions = imageService.rearangeDifferentPixelsToRegions(pixels);

            List<ChangeScope> scopes = null;
            for (List<Pixel> pixelsRegion : pixelsRegions) {
                if (pixelsRegion.size() > BOUNDARY) {
                    if (scopes == null) {
                        scopes = new ArrayList<>();
                    }
                    scopes.add(imageService.makeScope(pixelsRegion));
                }
            }

            WritableImage wImage = imageService.markChangedZones(secondImage.getImage(), scopes);

            imageService.displayChangedImage(secondImagePane, wImage);

        } catch (ImagesComparatorException e) {
            createPopUp(e.getTitle(), e.getMessage());
        }
    }

    private boolean isPathShouldBeHandled(final TextField imageLocation) {
        return imageLocation.getText() == null || imageLocation.getText().isEmpty() || Utils.isFileHasImageExtension(imageLocation.getText());
    }

    private void createPopUp(final String title, final String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.showAndWait();
    }

    private String getAllowedFileExtensionsMessage() {
        if (allowedFileExtensionsMessage == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(MSG_ALLOWED_EXTENSIONS_PART.getValue());
            for (Extensions extension : Extensions.values()) {
                sb.append(extension).append(COMMA.getValue());
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append(DOT.getValue());
            allowedFileExtensionsMessage = sb.toString();
            return allowedFileExtensionsMessage;
        } else {
            return allowedFileExtensionsMessage;
        }
    }
}
