package task.utils;

import task.constants.Extensions;

import static task.constants.MessagesComponents.DOT;

public interface Utils {

    static boolean isFileHasImageExtension(String pathToFile) {
        boolean result = false;
        int index = pathToFile.lastIndexOf(DOT.getValue());
        String fileExtension = null;
        if (pathToFile.length() > index + 1) {
            fileExtension = pathToFile.substring(pathToFile.lastIndexOf(DOT.getValue()) + 1);
        }

        for (Extensions extension : Extensions.values()) {
            if (extension.getValue().equalsIgnoreCase(fileExtension)) {
                result = true;
                return result;
            }
        }
        return result;
    }

    static Extensions getFileExtension(String pathToFile) {
        int index = pathToFile.lastIndexOf(DOT.getValue());
        Extensions result = null;
        String fileExtension = null;
        if (pathToFile.length() > index + 1) {
            fileExtension = pathToFile.substring(pathToFile.lastIndexOf(DOT.getValue()) + 1);
        }

        for (Extensions extension : Extensions.values()) {
            if (extension.getValue().equalsIgnoreCase(fileExtension)) {
                result = extension;
            }
        }
        return result;
    }
}
