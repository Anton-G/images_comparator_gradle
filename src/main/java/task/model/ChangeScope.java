package task.model;

public class ChangeScope {

    int maxX;

    int minX;

    int maxY;

    int minY;

    public ChangeScope(int maxX, int minX, int maxY, int minY) {
        this.maxX = maxX;
        this.minX = minX;
        this.maxY = maxY;
        this.minY = minY;
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    public int getMinX() {
        return minX;
    }

    public void setMinX(int minX) {
        this.minX = minX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }

    public int getMinY() {
        return minY;
    }

    public void setMinY(int minY) {
        this.minY = minY;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Region{");
        sb.append("maxX=").append(maxX);
        sb.append(", minX=").append(minX);
        sb.append(", maxY=").append(maxY);
        sb.append(", minY=").append(minY);
        sb.append('}');
        return sb.toString();
    }
}
