package task.model;


import javafx.scene.image.Image;
import task.constants.Extensions;

public class TestedImage {

    private String imagePath;

    private Extensions fileExtension;

    private Image image;

    public TestedImage(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setFileExtension(Extensions fileExtension) {
        this.fileExtension = fileExtension;
    }
}
