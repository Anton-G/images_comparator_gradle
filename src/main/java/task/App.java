package task;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/ImageComparator.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root, 800, 500);

        stage.setTitle("Image Comparator");
        stage.setScene(scene);
        stage.show();
    }
}
