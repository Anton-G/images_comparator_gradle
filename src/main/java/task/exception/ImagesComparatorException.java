package task.exception;

public class ImagesComparatorException extends Exception {
    private String title;

    public ImagesComparatorException(String title, String text) {
        super(text);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
